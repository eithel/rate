package main

import "time"

// pocket is a struct to keep track of how many calls we have made.
// A function should ask a coin using getCoin, and then, after the work is done,
// release the coin using freeCoin.
type pocket struct {
	coins chan struct{}
	free  chan int
	limit time.Duration
}

// freeCoin frees a coin
func (p *pocket) freeCoin(val int) {
	p.free <- val
}

// getCoin asks for a coin to use
func (p *pocket) getCoin() {
	<-p.coins
}

// mux fill a number of coins after every tick.
// The number of how many coins it dispenses, is a direct proportion of how many times freeCoin has been called.
// Tick is the time during which maximum number of calls are allowed.
func (p *pocket) mux() {
	tick := time.Tick(p.limit)
	freeCoins := 0

	for {
		select {
		case <-tick:
			// log.Println("tick! fill", freeCoins, "coins")
			// fmt.Printf("\n\n")

			// We fill only the freeCoins even if the channel could receive more coins.
			// This is done to keep call under the rete limit: some api calls could last for multiple ticks.
			for i := 0; i < freeCoins; i++ {
				p.coins <- struct{}{}
			}
			freeCoins = 0
		case <-p.free:
			// log.Println("free a coin!", v)
			freeCoins++
		}
	}

}

// newPocket create a new pocket.
// It accepts the limit of maximun call and a duration. This is the api's rate limit
func newPocket(limit int, d time.Duration) *pocket {

	if limit < 1 || d < 0 {
		return nil
	}

	r := &pocket{
		coins: make(chan struct{}, limit),
		free:  make(chan int),
		limit: d,
	}

	// Fill the pocket.
	for i := 0; i < limit; i++ {
		r.coins <- struct{}{}
	}

	// Run the mux
	go r.mux()

	return r
}
