package main

import (
	"log"
	"math/rand"
	"time"
)

func main() {

	p := newPocket(15, 1*time.Second)
	staffToDo := make([]int, 400)

	input, output := apiCall()

	for i := range staffToDo {

		// Try to get a coin.
		// This function blocks until some coins is available.
		p.getCoin()

		go func(val int) {
			input <- struct{}{} // Just for debug.

			// Simulate some work.
			d := time.Duration(random(200, 1500)) * time.Millisecond
			log.Println("Doing some work with", val, "for time", d)
			time.Sleep(d)

			output <- struct{}{} // Just for debug.

			// Free a coin
			p.freeCoin(val)
		}(i)

	}

}

// random generates a random number between min and max.
func random(min int64, max int64) int64 {
	return rand.Int63n(max-min) + min
}

// apiCall is used to track how many calls we are currently doing to the end-point.
func apiCall() (chan struct{}, chan struct{}) {
	w := make(chan struct{})
	done := make(chan struct{})
	current := 0

	go func() {
		for {
			select {
			case <-w:
				current++
				log.Println("api calls", current)
			case <-done:
				current--
				log.Println("api calls", current)
			}
		}
	}()

	return w, done
}
